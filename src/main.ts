import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import routes from 'virtual:generated-pages'
import App from './App.vue'

import vuetify from './plugins/vuetify' // add vuetify plugin
import { loadFonts } from './plugins/webfontloader' // add webfontloader plugin


import '@unocss/reset/tailwind.css'
import './styles/main.css'
import 'uno.css'

loadFonts()

const app = createApp(App)
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
})
app.use(router)
app.use(vuetify)
app.mount('#app')
